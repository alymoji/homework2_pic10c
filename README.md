# Homework 3 - Alyson Jimenez
Included: QT file, README file

This project was actually a lot of fun as I was able to use a different program and work with something that outputted something other than a black and white screen. 

In class, I truly believed that I understood the concepts until I tried it myself and did not know at all what to do. After watching multiple youtube tutorials and googling, I was able to figure out what to with QT designer. 
The issue with QT designer that I had was making sure everything was the appropriately labeled and that understanding that what I label is what I implement into my code. 

At first when I did this project, I deleted the grade_calculator.pro file assuming that I would work with a .ui file and aboslutely nothing made sense. I now understand the importance of the .pro file in that it connect the QT Designer with the code as that specific file tells the Desginer of the target which is the project. 

In the grade_calculator.h file, I primarily used the professor's code at the end. In the beginning I thought there needed to be more added for example, overriding operators. I tried using that method but in the process of using it, I completely wiped out my code. In the end, I realized that the only function I truly needed besides the copy constructor and destructor was a computing function. 

In the grade_calculator.cpp file, I refered to the lecture notes but not until very much later did I realize that the copy constructor initializes everything rather than having seperate functions to do so. I read online that it is much safer to code the SpinBoxes to have a maximum of 100 in that there are no issues in other functions. I learned a lot about SIGNAL and SLOT in this function. I assumed that you had to create a function that is put in both the SIGNAL and SLOT. The SIGNAL indicates what the user is doing so it can be any given function that shows the buttons or slides have been clicked. SLOT is the function where the widgets will be applied to. Struggling with getting the RadioButton conencts to work, I found it very simple to connect the slides and spinboxes. In the compute_overall() function, the hardest part was actually the math. Learning about QString and how to connect it the output by SetText() took a lot longer for me to acknowledge. After working with that function I realized that widgets have their own specific functions rather than random ones so after googling the functions of each one, it made the rest of the project really simple. With Qstring, I thought there was a QInt but there were no functions related to it so on a whim I tried QString and found a function that makes it a number. 

In the main function, I am so used to putting a lot of work into it but it turned out to be really short. Thinking about what actually needed to be done, mostly everything is in the grade_calculator.cpp/.h files. 

Overall, I am definitely going to try out projects with QT in the future as I can see it being very helpful in future projects. 
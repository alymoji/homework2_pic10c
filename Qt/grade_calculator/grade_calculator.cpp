#include "grade_calculator.h"
#include "ui_grade_calculator.h"

grade_calculator::grade_calculator(QWidget *parent)
    : QMainWindow(parent), ui(new Ui::grade_calculator)
{
    ui->setupUi(this);

    //connects RadioButton
    QObject::connect(ui->schemaA, SIGNAL(clicked()), this, SLOT(compute_overall()));
    QObject::connect(ui->schemaB, SIGNAL(clicked()), this, SLOT(compute_overall()));

    //default max of SpinBox is 99 but need to set them to 100
    ui->hw1spin -> setMaximum(100);
    ui->hw2spin -> setMaximum(100);
    ui->hw3spin -> setMaximum(100);
    ui->hw4spin -> setMaximum(100);
    ui->hw5spin -> setMaximum(100);
    ui->hw6spin -> setMaximum(100);
    ui->hw7spin -> setMaximum(100);
    ui->hw8spin -> setMaximum(100);
    ui->m1spin -> setMaximum (100);
    ui->m2spin -> setMaximum(100);
    ui->finalexspin -> setMaximum(100);

    //connects spinbox to slider and vice versa
    QSpinBox::connect(ui->hw1spin, SIGNAL(valueChanged(int)), ui->hw1slide, SLOT(setValue(int)));
    QSpinBox::connect(ui->hw2spin, SIGNAL(valueChanged(int)), ui->hw2slide, SLOT(setValue(int)));
    QSpinBox::connect(ui->hw3spin, SIGNAL(valueChanged(int)), ui->hw3slide, SLOT(setValue(int)));
    QSpinBox::connect(ui->hw4spin, SIGNAL(valueChanged(int)), ui->hw4slide, SLOT(setValue(int)));
    QSpinBox::connect(ui->hw5spin, SIGNAL(valueChanged(int)), ui->hw5slide, SLOT(setValue(int)));
    QSpinBox::connect(ui->hw6spin, SIGNAL(valueChanged(int)), ui->hw6slide, SLOT(setValue(int)));
    QSpinBox::connect(ui->hw7spin, SIGNAL(valueChanged(int)), ui->hw7slide, SLOT(setValue(int)));
    QSpinBox::connect(ui->hw8spin, SIGNAL(valueChanged(int)), ui->hw8slide, SLOT(setValue(int)));
    QSpinBox::connect(ui->m1spin, SIGNAL(valueChanged(int)), ui->m1slider, SLOT(setValue(int)));
    QSpinBox::connect(ui->m2spin, SIGNAL(valueChanged(int)), ui->m2slider, SLOT(setValue(int)));
    QSpinBox::connect(ui->finalexspin, SIGNAL(valueChanged(int)), ui->finalexslider, SLOT(setValue(int)));

    QSlider::connect(ui->hw1slide, SIGNAL(valueChanged(int)), ui->hw1spin, SLOT(setValue(int)));
    QSlider::connect(ui->hw2slide, SIGNAL(valueChanged(int)), ui->hw2spin, SLOT(setValue(int)));
    QSlider::connect(ui->hw3slide, SIGNAL(valueChanged(int)), ui->hw3spin, SLOT(setValue(int)));
    QSlider::connect(ui->hw4slide, SIGNAL(valueChanged(int)), ui->hw4spin, SLOT(setValue(int)));
    QSlider::connect(ui->hw5slide, SIGNAL(valueChanged(int)), ui->hw5spin, SLOT(setValue(int)));
    QSlider::connect(ui->hw6slide, SIGNAL(valueChanged(int)), ui->hw6spin, SLOT(setValue(int)));
    QSlider::connect(ui->hw7slide, SIGNAL(valueChanged(int)), ui->hw7spin, SLOT(setValue(int)));
    QSlider::connect(ui->hw8slide, SIGNAL(valueChanged(int)), ui->hw8spin, SLOT(setValue(int)));
    QSlider::connect(ui->m1slider, SIGNAL(valueChanged(int)), ui->m1spin, SLOT(setValue(int)));
    QSlider::connect(ui->m2slider, SIGNAL(valueChanged(int)), ui->m2spin, SLOT(setValue(int)));
    QSlider::connect(ui->finalexslider, SIGNAL(valueChanged(int)), ui->finalexspin, SLOT(setValue(int)));

}

grade_calculator::~grade_calculator()
{
    delete ui;
}

void grade_calculator::compute_overall()
{
    double avgHW;
    double MidtermA;
    double MidtermB;
    double finalexamA;
    double finalexamB;


    QString display;

    //average hw
    int hw1 = ui->hw1spin -> value();
    int hw2 = ui->hw2spin -> value();
    int hw3 = ui->hw3spin -> value();
    int hw4 = ui->hw4spin -> value();
    int hw5 = ui->hw5spin -> value();
    int hw6 = ui->hw6spin -> value();
    int hw7 = ui->hw7spin -> value();
    int hw8 = ui->hw8spin -> value();
    int hwScore = hw1 + hw2 + hw3 + hw4 + hw5 + hw6 + hw7 + hw8;
    avgHW = ((hwScore/800))*0.25;

    //average midterm
    int mid1 = ui->m1spin -> value();
    int mid2 = ui->m2spin -> value();
    MidtermA = mid1*0.2 + mid2*0.2;
    if(mid1 > mid2)
    {
        MidtermB = mid1*0.30;
    }
    else
    {
        MidtermB = mid2*0.30;
    }

    //final exam
    int fintest = ui->finalexspin -> value();
    finalexamA = double(fintest*0.30);
    finalexamB = double(fintest*0.44);

    //finding which scheme to use
    if(ui->schemaA->isChecked())
    {
        display = QString::number(avgHW + MidtermA + finalexamA, 'F', 2);
    }

    else if(ui->schemaB->isChecked())
    {
        display = QString::number(avgHW + MidtermB + finalexamB, 'F', 2);
    }

    ui->FinalScorelabel -> setText(display); //outputs final score
}







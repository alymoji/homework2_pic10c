#-------------------------------------------------
#
# Project created by QtCreator 2019-03-04T21:20:01
#
#-------------------------------------------------

QT += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = grade_calculator
TEMPLATE = app

DEFINES += QT_DEPRECATED_WARNINGS

SOURCES += \
    main.cpp \
    grade_calculator.cpp

HEADERS += \
    grade_calculator.h

FORMS +=  \
    grade_calculator.ui


